module.exports = {
	content: ["./src/**/*.{html,js,css}"],
	theme: {
		extend: {
			fontFamily: {
				"spc-inter": ["Inter"],
				"spc-ld": ["Lexend Deca"]
			},
			colors: {
				"spc-main-bg": "hsl(233, 47%, 7%)",
				"spc-card-bg": "hsl(244, 38%, 16%)",
				"spc-accent": "hsl(277, 64%, 61%)",
				"spc-accent-500": "hsla(277, 100%, 20%, 0.65)",
				"spc-white": "hsl(0, 0%, 100%)",
				"spc-main-p": "hsla(0, 0%, 100%, 0.75)",
				"spc-heading-s": "hsla(0, 0%, 100%, 0.6)"
			}
		},
	},
	plugins: [],
}
